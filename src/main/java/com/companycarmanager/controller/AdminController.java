package com.companycarmanager.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.companycarmanager.dto.CarDto;
import com.companycarmanager.dto.CarModelDto;
import com.companycarmanager.dto.UserDto;
import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.CarManufacturer;
import com.companycarmanager.persistence.domain.Position;
import com.companycarmanager.persistence.domain.Refueling;
import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.persistence.repository.PositionRepository;
import com.companycarmanager.service.CarService;
import com.companycarmanager.service.IUserService;
import com.companycarmanager.service.PositionService;
import com.companycarmanager.service.RefuelingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping(value = "/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AdminController {
	@Autowired
	private IUserService userService;

	@Autowired
	private CarService carService;

	@Autowired
	private RefuelingService refuelingService;

	@Autowired
	private PositionService positionService;

	@RequestMapping(value = "/users/list", method = RequestMethod.GET)
	ModelAndView showUserList(ModelAndView model) {
		model.addObject("userList", userService.getAll());
		model.setViewName("user_list");
		return model;
	}

	@RequestMapping(value = "/users/details/{userName}", method = RequestMethod.GET)
	String showEditUser(
			@PathVariable(value = "userName") @NotNull String userName,
			ModelMap model, RedirectAttributes redirectAttributes) {
		User user = null;
		try {
			user = (User) userService.loadUserByUsername(userName);
		} catch (UsernameNotFoundException e) {
			redirectAttributes.addFlashAttribute("message",
					"User does not exist.");
			return "redirect:/admin/users/list";
		}

		model.addAttribute("cars", userService.findAssignedCars(userName));
		model.addAttribute("user", user);
		model.addAttribute("refuelings",
				refuelingService.getRefuelingsByUser(user));
		model.addAttribute("positions",
				positionService.getPositionsByUser(user));
		return "user_details";
	}

	@RequestMapping(value = "/users/edit/{userName}", method = RequestMethod.GET)
	String showEditUser(
			@PathVariable(value = "userName") @NotNull String userName,
			UserDto userDto, ModelMap model,
			RedirectAttributes redirectAttributes) {
		User user = null;
		try {
			user = (User) userService.loadUserByUsername(userName);
		} catch (UsernameNotFoundException e) {
			redirectAttributes.addFlashAttribute("message",
					"User does not exist.");
			return "redirect:/admin/users/list";
		}
		userDto.fillFromUser(user);
		Set<Car> cars = carService.getAllCarsWithoutCurrentUser();
		if (user.getCurrentCar() != null) {
			cars.add(user.getCurrentCar());
		}
		model.addAttribute("cars", cars);
		return "user_edit";
	}

	@RequestMapping(value = "/users/edit", method = RequestMethod.POST)
	ModelAndView editUser(UserDto userDto, ModelAndView model,
			BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		Car currentCar = userDto.getCurrentCarId() != null ? carService
				.findById(userDto.getCurrentCarId()) : null;
		if (!userDto.getEmail().isEmpty()
				&& userService.updateUser(userDto, currentCar)
				&& !bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("message",
					"User sucessfully edited.");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Invalid user data or user does not exist.");
		}

		model.setViewName("redirect:/admin/users/list");
		return model;
	}

	@RequestMapping(value = "/users/add", method = RequestMethod.GET)
	String showAddUser(UserDto userDto) {
		return "user_add";
	}

	@RequestMapping(value = "/users/add", method = RequestMethod.POST)
	String addUser(@Valid UserDto userDto, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("message",
					"Invalid user data.");
			return "redirect:/admin/users/add";
		}

		if (userService.addUser(userDto)) {
			redirectAttributes.addFlashAttribute("message",
					"User successfully added");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"User name or email already exists.");
		}

		return "redirect:/admin/users/list";
	}

	@RequestMapping(value = "/users/assigned_cars/{userName}", method = RequestMethod.GET)
	ModelAndView showAssignedCars(@PathVariable("userName") String userName,
			ModelAndView model) {
		model.addObject("cars", userService.findAssignedCars(userName));
		model.setViewName("user_assigned_cars");
		return model;
	}

	@RequestMapping(value = "/cars/add_manufacturer", method = RequestMethod.GET)
	ModelAndView showAddCarManufactruer(CarManufacturer manufacturer,
			ModelAndView model) {
		model.addObject("manufacturer", manufacturer);
		model.setViewName("car_manufacturer_add");
		return model;
	}

	@RequestMapping(value = "/cars/add_manufacturer", method = RequestMethod.POST)
	ModelAndView addCarManufactuer(@Valid CarManufacturer manufacturer,
			ModelAndView model, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {

		if (!bindingResult.hasErrors()
				&& carService.createCarManufacturer(manufacturer)) {
			redirectAttributes.addFlashAttribute("message",
					"Manufacturer successfully added.");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Manufacturer already exists or empty name.");
		}

		model.setViewName("redirect:/admin/cars/list");
		return model;
	}

	@RequestMapping(value = "/cars/add_model", method = RequestMethod.GET)
	ModelAndView showAddCarModel(CarModelDto modelDto, ModelAndView model) {
		model.addObject("carManufacturers", carService.getAllCarManufacturers());
		model.addObject("modelDto", modelDto);
		model.setViewName("car_model_add");
		return model;
	}

	@RequestMapping(value = "/cars/add_model", method = RequestMethod.POST)
	ModelAndView addCarModel(@Valid CarModelDto modelDto, ModelAndView model,
			BindingResult bindingResult, RedirectAttributes redirectAttributes) {

		if (!bindingResult.hasErrors() && carService.createCarModel(modelDto)) {
			redirectAttributes.addFlashAttribute("message",
					"Car model successfully added");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Car model already exists or invalid name/manufacturer.");
		}

		model.setViewName("redirect:/admin/cars/list");
		return model;
	}

	@RequestMapping(value = "/cars/edit/{carId}", method = RequestMethod.GET)
	ModelAndView showEditCar(@PathVariable("carId") @NotNull Integer carId,
			CarDto carDto, ModelAndView model) {
		Car car = carService.findById(carId);
		carDto.fillFromCar(car);
		model.addObject("carModels", carService.getAllCarModels());
		model.setViewName("car_edit");
		return model;
	}

	@RequestMapping(value = "/cars/edit/{carId}", method = RequestMethod.POST)
	String editCar(@PathVariable("carId") @NotNull Integer carId,
			@Valid CarDto carDto, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		carDto.setId(carId);
		if (!bindingResult.hasErrors() && carService.updateCar(carDto)) {
			redirectAttributes.addFlashAttribute("message",
					"Car successfully edited.");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Invalid car data or registration number already used.");
		}

		return "redirect:/admin/cars/list";
	}

	@RequestMapping(value = "/cars/add", method = RequestMethod.GET)
	ModelAndView showAddCar(CarDto carDto, ModelAndView model) {
		model.addObject("carModels", carService.getAllCarModels());
		model.setViewName("car_add");
		return model;
	}

	@RequestMapping(value = "/cars/add", method = RequestMethod.POST)
	String addCar(@Valid CarDto carDto, RedirectAttributes redirectAttributes) {
		if (carService.addCar(carDto)) {
			redirectAttributes.addFlashAttribute("message",
					"Car successfully added.");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Invalid car data or registration number already used.");
		}

		return "redirect:/admin/cars/list";
	}

	@RequestMapping(value = "/cars/list", method = RequestMethod.GET)
	ModelAndView showCarList(ModelAndView model) {
		model.addObject("carList", carService.getAll());
		model.setViewName("car_list");
		return model;
	}

	@RequestMapping(value = "/cars/details/{carID}", method = RequestMethod.GET)
	ModelAndView showCarDetails(
			@PathVariable(value = "carID") @NotNull Integer carId,
			ModelAndView model, RedirectAttributes redirectAttributes) {
		Car car = carService.findById(carId);
		if (car != null) {
			List<Position> positions = positionService.getPositionsByCar(car);
			model.addObject("car", car);
			model.addObject("currentPosition", positions.isEmpty() ? null
					: positions.get(0));
			model.addObject("refuelings",
					refuelingService.getRefuelingsByCar(car));
			model.addObject("positions", positions);
			model.setViewName("car_details");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Car does not exist.");
			model.setViewName("redirect:/admin/cars/list");
		}

		return model;
	}

	@RequestMapping(value = "/cars/assign_by_id/{carID}", method = RequestMethod.GET)
	ModelAndView showAssignCar(
			@PathVariable(value = "carID") @NotNull Integer carId,
			UserDto userDto, ModelAndView model,
			RedirectAttributes redirectAttributes) {
		Car car = carService.findById(carId);
		if (car != null && car.isAvailable()) {
			model.addObject("users",
					userService.getUsersWithoutCarAssigned(car));
			model.addObject("car", car);
			userDto.setPassword("password");
			userDto.setEmail("email@email.com");
			model.setViewName("car_assign");
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Car is no longer available.");
			model.setViewName("redirect:/admin/cars/list");
		}
		return model;
	}

	@RequestMapping(value = "/cars/assign_by_id/{carID}", method = RequestMethod.POST)
	ModelAndView assignCar(
			@PathVariable(value = "carID") @NotNull Integer carId,
			UserDto userDto, ModelAndView model, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {

		Car car = carId != null ? carService.findById(carId) : null;
		if (!bindingResult.hasErrors() && car != null) {
			try {
				User user = (User) userService.loadUserByUsername(userDto
						.getUserName());
				user.getAvailableCars().add(car);
				userService.saveUser(user);
			} catch (UsernameNotFoundException e) {
				redirectAttributes.addFlashAttribute("message",
						"User does not exist.");
			}
		} else {
			redirectAttributes.addFlashAttribute("message",
					"Car does not exist or invalid form data.");
		}

		model.setViewName("redirect:/admin/cars/list");
		return model;
	}

	@RequestMapping(value = "/cars/remove_assignment/{carID}/from_user/{userName}", method = RequestMethod.POST)
	ModelAndView removeCarAssignment(
			@PathVariable("carID") @NotNull Integer carId,
			@PathVariable("userName") @NotNull String userName,
			ModelAndView model, RedirectAttributes redirectAttributes) {
		Car car = carService.findById(carId);
		try {
			User user = (User) userService.loadUserByUsername(userName);
			if (userService.removeCarAssignment(user, car)) {

			} else {
				redirectAttributes.addFlashAttribute("message",
						"Car no longer assigned to user.");
			}
		} catch (UsernameNotFoundException e) {
			redirectAttributes.addFlashAttribute("message",
					"User does not exist.");
		}

		redirectAttributes.addFlashAttribute("message",
				"Successfully removed car assignment.");
		model.setViewName("redirect:/admin/users/assigned_cars/" + userName);
		return model;
	}

	@RequestMapping(value = "/positions/show/{positionId}", method = RequestMethod.GET)
	String showPosition(
			@PathVariable("positionId") @NotNull Integer positionId,
			ModelMap model, RedirectAttributes redirectAttributes) {
		Position position = positionService.findById(positionId);
		if (position == null) {
			redirectAttributes.addFlashAttribute("message",
					"Position does not exist.");
			return "redirect:/index";
		} else {
			model.addAttribute("position", position);
			return "position_show";
		}
	}
	
	@RequestMapping(value="/cars/show_current_positions", method = RequestMethod.GET)
	String showCurrentCarPositions(ModelMap model) {
		Set<Car> cars = carService.getAllEnabledCars();
		Set<Position> positions = positionService.getCurrentCarPositions(cars);

		String positionsJSON = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			positionsJSON = mapper.writeValueAsString(positions);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "redirect:/index";
		}
		model.addAttribute("positions", positionsJSON);
		return "car_current_positions";
	}
}
