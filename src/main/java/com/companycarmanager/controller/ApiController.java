package com.companycarmanager.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.companycarmanager.dto.PositionDto;
import com.companycarmanager.dto.RefuelingDto;
import com.companycarmanager.dto.RestCarDto;
import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.Position;
import com.companycarmanager.persistence.domain.Refueling;
import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.service.CarService;
import com.companycarmanager.service.IUserService;
import com.companycarmanager.service.PositionService;
import com.companycarmanager.service.RefuelingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api")
public class ApiController {
	@Autowired
	private RefuelingService refuelingService;

	@Autowired
	private PositionService positionService;

	@Autowired
	private CarService carService;

	@Autowired
	private IUserService userService;

	@Autowired
	private AuthenticationManager authenticationManager;

	class PositionId {
		public PositionId() {
		}

		public PositionId(Integer id) {
			positionId = id;
		}

		public Integer positionId;
	}

	class RestRequest {
		private JsonNode node;
		private ObjectMapper mapper;

		RestRequest(String data) throws JsonProcessingException, IOException {
			mapper = new ObjectMapper();
			node = mapper.readTree(data);
		}

		String getUsername() {
			return mapper.convertValue(node.get("userName"), String.class);
		}

		String getPassword() {
			return mapper.convertValue(node.get("password"), String.class);
		}

		<T> T getObject(String name, Class<T> clazz) {
			return mapper.convertValue(node.get(name), clazz);
		}
	}

	@RequestMapping(value = { "/cars/select" }, method = RequestMethod.POST)
	ResponseEntity<String> selectCar(@RequestBody @NotNull String data) {
		RestRequest request;
		try {
			request = new RestRequest(data);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		Authentication authentication = null;

		try {
			authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							request.getUsername(), request.getPassword()));
		} catch (BadCredentialsException e) {
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}

		User user = (User) authentication.getPrincipal();
		Integer carId = request.getObject("carId", Integer.class);

		Car car = (carId != null && carId.intValue() > 0) ? carService
				.findById(carId) : null;

		if (!user.hasAuthority("ROLE_USER")) {
			return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
		}

		if (userService.setUsersCurrentCar(user, car)) {
			return new ResponseEntity<String>(HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(value = { "/refuelings/add" }, method = RequestMethod.POST)
	ResponseEntity<String> addNewRefueling(@RequestBody String data) {
		RestRequest request;
		try {
			request = new RestRequest(data);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		Authentication authentication = null;

		try {
			authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							request.getUsername(), request.getPassword()));
		} catch (BadCredentialsException e) {
			return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
		}

		User user = (User) authentication.getPrincipal();

		if (!user.hasAuthority("ROLE_USER")) {
			return new ResponseEntity<String>(HttpStatus.FORBIDDEN);
		}

		RefuelingDto dto = request.getObject("refueling", RefuelingDto.class);

		Position position = positionService.findById(dto.getPositionId());

		if (position == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}

		if (position.getUser().getUsername() != user.getUsername()) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}

		Refueling refueling = new Refueling();

		refueling.setAmount(dto.getAmount());
		refueling.setCostPerLitre(dto.getCostPerLitre());
		refueling.setPosition(position);

		refuelingService.save(refueling);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@RequestMapping(value = { "/positions/add" }, method = RequestMethod.POST)
	ResponseEntity<PositionId> addNewPosition(@RequestBody String data) {
		RestRequest request;
		try {
			request = new RestRequest(data);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new ResponseEntity<PositionId>(HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<PositionId>(HttpStatus.BAD_REQUEST);
		}

		Authentication authentication = null;

		try {
			authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							request.getUsername(), request.getPassword()));
		} catch (BadCredentialsException e) {
			return new ResponseEntity<PositionId>(HttpStatus.UNAUTHORIZED);
		}

		User user = (User) authentication.getPrincipal();

		if (!user.hasAuthority("ROLE_USER")) {
			return new ResponseEntity<PositionId>(HttpStatus.FORBIDDEN);
		}

		PositionDto dto = request.getObject("position", PositionDto.class);

		/*
		 * Validator validator =
		 * Validation.buildDefaultValidatorFactory().getValidator();
		 * 
		 * Set<ConstraintViolation<PositionDto>> violations =
		 * validator.validate(dto, PositionDto.class);
		 * 
		 * if (!violations.isEmpty()) { return new
		 * ResponseEntity<Integer>(HttpStatus.BAD_REQUEST); }
		 */

		Car car = carService.findById(dto.getCarId());

		if (car == null || user == null) {
			return new ResponseEntity<PositionId>(HttpStatus.NOT_FOUND);
		}

		if (!user.getUsername().equals(dto.getUserName())
				|| user.getCurrentCar().getId() != car.getId()) {
			return new ResponseEntity<PositionId>(HttpStatus.BAD_REQUEST);
		}

		Position position = new Position();
		position.setCar(car);
		position.setUser(user);
		position.setLatitude(dto.getLatitude());
		position.setLongitude(dto.getLongitude());
		position.setDate(dto.getDate());

		positionService.save(position);

		return new ResponseEntity<PositionId>(new PositionId(position.getId()),
				HttpStatus.OK);
	}

	@RequestMapping(value = { "/cars/available" }, method = RequestMethod.POST)
	public ResponseEntity<List<RestCarDto>> getAllAvailableCars(
			@RequestBody String data) {
		RestRequest request;
		try {
			request = new RestRequest(data);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new ResponseEntity<List<RestCarDto>>(HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<List<RestCarDto>>(HttpStatus.BAD_REQUEST);
		}

		Authentication authentication = null;

		try {
			authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							request.getUsername(), request.getPassword()));
		} catch (BadCredentialsException e) {
			return new ResponseEntity<List<RestCarDto>>(HttpStatus.UNAUTHORIZED);
		}

		User user = (User) authentication.getPrincipal();

		if (!user.hasAuthority("ROLE_USER")) {
			return new ResponseEntity<List<RestCarDto>>(HttpStatus.FORBIDDEN);
		}

		Set<Car> cars = carService.getAllCarsCurrentlyAvailableToUser(user);
		List<RestCarDto> dtos = new ArrayList<RestCarDto>();
		if (user.getCurrentCar() != null) {
			cars.add(user.getCurrentCar());
		}
		for (Car car : cars) {
			RestCarDto dto = new RestCarDto(car);
			dtos.add(dto);
		}

		return new ResponseEntity<List<RestCarDto>>(dtos, HttpStatus.OK);
	}
}
