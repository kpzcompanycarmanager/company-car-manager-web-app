package com.companycarmanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.companycarmanager.dto.UserDto;
import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.persistence.repository.UserRepository;
import com.companycarmanager.persistence.repository.UserRoleRepository;

@Service
public class UserService implements IUserService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	UserDetails user = userRepository.findOne(username);
    	if (user == null) {
    		throw new UsernameNotFoundException("User not found");
    	}
    	return user;
    }
	
	@Transactional
	public boolean addUser(UserDto userDto) {
		User user = userRepository.findOne(userDto.getUserName());
		
		if (user != null || userRepository.findUserByEmail(userDto.getEmail()) != null) {
			return false;
		}
		
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		String encodedPassword = encoder.encodePassword(userDto.getPassword(), null);
		user = new User(userDto.getUserName(), encodedPassword, userDto.getEmail(), userDto.isEnabled());
		user.getRoles().add(userRoleRepository.findUserRoleByName("ROLE_USER"));
		userRepository.save(user);
		return true;
	}
	
	@Transactional
	public boolean updateUser(UserDto userDto, Car currentCar) {
		User user = userRepository.findOne(userDto.getUserName());
		
		if (user == null || userRepository.findUserByEmail(userDto.getEmail()).getUsername() != userDto.getUserName()) {
			return false;
		}
		user.setCurrentCar(currentCar);
		user.setEmail(userDto.getEmail());
		user.setEnabled(userDto.isEnabled());
		
		if (!userDto.getPassword().isEmpty()) {
			ShaPasswordEncoder encoder = new ShaPasswordEncoder();
			String encodedPassword = encoder.encodePassword(userDto.getPassword(), null);
			user.setPassword(encodedPassword);
		}
		userRepository.save(user);
		return true;
	}
	
	@Transactional(readOnly = true)
	public List<User> getAll() {
		return userRepository.findAll();
	}
	
	@Transactional(readOnly = true)
	public List<User> getUsersWithoutCarAssigned(Car car) {
		List<User> l = userRepository.findUsersWithoutCarAssigned(car.getId());
		return l;
	}

	@Transactional
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Transactional(readOnly = true)
	public List<Car> findAssignedCars(String userName) {
		return userRepository.findAssignedCars(userName);
	}
	
	@Transactional(readOnly = true)
	public boolean checkUserExists(String userName) {
		return userRepository.findOne(userName) != null;
	}

	@Transactional
	public boolean removeCarAssignment(User user, Car car) {
		boolean removed  = user.getAvailableCars().remove(car);
		userRepository.save(user);
		return removed;
	}

	@Transactional(readOnly = true)
	public User findByUserName(String userName) {
		return userRepository.findOne(userName);
	}
	
	@Transactional
	public boolean setUsersCurrentCar(User user, Car car) {
		if (car == null || (user.getAvailableCars().contains(car) && userRepository.findUserByCurrentCar(car) == null)) {
			user.setCurrentCar(car);
			userRepository.save(user);
			return true;
		} else {
			return false;
		}
	}

	
}
