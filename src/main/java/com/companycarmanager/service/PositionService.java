package com.companycarmanager.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.Position;
import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.persistence.repository.PositionRepository;

@Service
public class PositionService {
	@Autowired
	private PositionRepository positionRepository;

	@Transactional(readOnly = true)
	public List<Position> getPositionsByCar(Car car) {
		return positionRepository.findPositionByCarOrderByDateDesc(car);
	}

	@Transactional(readOnly = true)
	public List<Position> getPositionsByUser(User user) {
		return positionRepository.findPositionByUserOrderByDateDesc(user);
	}

	@Transactional(readOnly = true)
	public Position findById(Integer positionId) {
		return positionRepository.findOne(positionId);
	}

	@Transactional
	public void save(Position position) {
		positionRepository.save(position);
	}
	
	@Transactional(readOnly = true)
	public Set<Position> getCurrentCarPositions(Set<Car> cars) {
		 Set<Position> positions = positionRepository.findCarPositions(cars);
		 Set<Car> checkedCars = new HashSet<Car>();
		 Set<Position> ret = new HashSet<Position>();
		 for (Position position : positions) {
			 if (!checkedCars.contains(position.getCar())) {
				 checkedCars.add(position.getCar());
				 ret.add(position);
			 }
		 }
		 return ret;
	}
}
