package com.companycarmanager.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.companycarmanager.dto.UserDto;
import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.User;

public interface IUserService extends UserDetailsService {

	public abstract boolean addUser(UserDto userDto);
	public abstract boolean updateUser(UserDto userDto, Car currentCar);
	public abstract List<User> getAll();
	public abstract List<User> getUsersWithoutCarAssigned(Car car);
	public abstract void saveUser(User user);
	public abstract List<Car> findAssignedCars(String userName);
	public abstract boolean checkUserExists(String userName);
	public abstract boolean removeCarAssignment(User user, Car car);
	public abstract User findByUserName(String userName);
	public abstract boolean setUsersCurrentCar(User user, Car car);

}