package com.companycarmanager.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.companycarmanager.dto.CarDto;
import com.companycarmanager.dto.CarModelDto;
import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.CarManufacturer;
import com.companycarmanager.persistence.domain.CarModel;
import com.companycarmanager.persistence.domain.Position;
import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.persistence.repository.CarManufacturerRepository;
import com.companycarmanager.persistence.repository.CarModelRepository;
import com.companycarmanager.persistence.repository.CarRepository;

@Service
public class CarService {
	@Autowired
	private CarRepository repository;

	@Autowired
	private CarManufacturerRepository manufacturerRepository;

	@Autowired
	private CarModelRepository modelRepository;

	@Transactional(readOnly = true)
	public List<CarModel> getAllCarModels() {
		return modelRepository.findAll();
	}

	@Transactional
	public void save(Car car) {
		repository.save(car);
	}

	@Transactional(readOnly = true)
	public Car findById(Integer id) {
		return repository.findOne(id);
	}

	@Transactional(readOnly = true)
	public List<Car> getAll() {
		return repository.findAll();
	}

	@Transactional
	public boolean updateCar(CarDto carDto) {
		Car car = repository.findOne(carDto.getId());
		Car foundByRegNumber = repository.findCarByRegistrationNumber(carDto
				.getRegistrationNumber());
		CarModel carModel = modelRepository.findOne(carDto.getModelId());
		if (car != null && carModel != null && (foundByRegNumber == car || foundByRegNumber == null)) {
			car.setFromDto(carDto);
			car.setModel(carModel);
			repository.save(car);
			return true;
		} else {
			return false;
		}
	}

	@Transactional
	public boolean addCar(CarDto carDto) {
		CarModel carModel = modelRepository.findOne(carDto.getModelId());
		Car car = repository.findCarByRegistrationNumber(carDto
				.getRegistrationNumber());
		if (carModel != null && car == null) {
			car = new Car(carDto.getRegistrationNumber(), carModel,
					carDto.getAvailable());
			repository.save(car);
			return true;
		} else {
			return false;
		}
	}

	@Transactional(readOnly = true)
	public boolean checkCarExists(Integer carId) {
		return repository.findOne(carId) != null;
	}

	@Transactional(readOnly = true)
	public List<CarManufacturer> getAllCarManufacturers() {

		return manufacturerRepository.findAll();
	}

	@Transactional
	public boolean createCarModel(CarModelDto model) {
		CarModel foundModel = modelRepository.findCarModelByName(model
				.getName());
		CarManufacturer manufacturer = manufacturerRepository.findOne(model
				.getManufacturerId());
		if (foundModel == null && manufacturer != null) {
			CarModel carModel = new CarModel();
			carModel.setName(model.getName());
			carModel.setManufacturer(manufacturer);
			modelRepository.save(carModel);
			return true;
		} else {
			return false;
		}

	}

	@Transactional
	public boolean createCarManufacturer(CarManufacturer manufacturer) {
		CarManufacturer foundManufacturer = manufacturerRepository
				.findCarManufacturerByName(manufacturer.getName());
		if (foundManufacturer == null) {
			manufacturerRepository.save(manufacturer);
			return true;
		} else {
			return false;
		}
	}

	@Transactional(readOnly = true)
	public Set<Car> getAllCarsWithoutCurrentUser() {
		return repository.findCarsWithoutCurrentUser();
	}
	
	@Transactional(readOnly = true)
	public Set<Car> getAllCarsCurrentlyAvailableToUser(User user) {
		return repository.findCarsWithoutCurrentUserAvailableToUser(user.getUsername());
	}
	
	@Transactional(readOnly = true)
	public Set<Car> getAllEnabledCars() {
		return repository.findCarByAvailableTrue();
	}
}
