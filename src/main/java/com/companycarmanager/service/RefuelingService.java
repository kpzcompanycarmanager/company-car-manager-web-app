package com.companycarmanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.companycarmanager.dto.RefuelingDto;
import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.Position;
import com.companycarmanager.persistence.domain.Refueling;
import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.persistence.repository.RefuelingRepository;

@Service
public class RefuelingService {
	@Autowired
	private RefuelingRepository repository;

	@Transactional(readOnly = true)
	public List<Refueling> getRefuelingsByCar(Car car) {
		return repository.findRefuelingByPositionCarOrderByPositionDateDesc(car);
	}
	
	@Transactional(readOnly = true)
	public List<Refueling> getRefuelingsByUser(User user) {
		return repository.findRefuelingByPositionUserOrderByPositionDateDesc(user);
	}
	
	@Transactional
	public void addRefueling(RefuelingDto dto, Position position) {
		Refueling refueling = new Refueling();
		refueling.setAmount(dto.getAmount());
		refueling.setCostPerLitre(dto.getCostPerLitre());
		refueling.setPosition(position);
		repository.save(refueling);
	}

	public void save(Refueling refueling) {
		repository.save(refueling);
	}
}
