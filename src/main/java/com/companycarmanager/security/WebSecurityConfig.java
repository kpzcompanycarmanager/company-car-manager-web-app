package com.companycarmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.companycarmanager.service.IUserService;
import com.mysql.jdbc.authentication.Sha256PasswordPlugin;


@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private IUserService userService;
	
	private RequestMatcher apiRequestMatcher = new ApiRequestMatcher();
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf()
        		.requireCsrfProtectionMatcher(apiRequestMatcher)
        		.and()
            .authorizeRequests()
            	.antMatchers("/api/**")
            		.permitAll()
            	.antMatchers("/img/**")
					.permitAll()
            	.antMatchers("/css/**")
					.permitAll()
				.antMatchers("/js/**")
					.permitAll()
				.antMatchers("/admin/**")
					.hasAuthority("ROLE_ADMIN")
                .anyRequest()
                	.authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            .logout()
                .permitAll();
    }

    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    	auth.userDetailsService(userService)
    		.passwordEncoder(new ShaPasswordEncoder());
    }
}