package com.companycarmanager.security;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;

public class ApiRequestMatcher implements RequestMatcher {
	private Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
 
    public boolean matches(HttpServletRequest request) {
    	//Disabe CSRF for http methods specified in allowed methods
    	//and for URIs starting with /api
    	if (allowedMethods.matcher(request.getMethod()).matches() || request.getRequestURI().toString().matches("^/api/.+")) {
    		return false;
    	} else {
    		return true;
    	}
    }

}
