package com.companycarmanager.persistence.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.companycarmanager.dto.CarDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Car {
	@Id
	@GeneratedValue
	private Integer id;

	@Column(unique = true, nullable = false)
	private String registrationNumber;
	@ManyToOne
	private CarModel model;

	@Column(nullable = false)
	boolean available = true;

	@JsonIgnore
	@ManyToMany(mappedBy = "availableCars")
	private Set<User> user = new HashSet<User>();

	public Car() {
	}

	public Car(String registrationNumber, CarModel model, boolean available) {
		this.registrationNumber = registrationNumber;
		this.model = model;
		this.available = available;
	}

	public void setFromDto(CarDto carDto) {
		registrationNumber = carDto.getRegistrationNumber();
		available = carDto.getAvailable();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CarModel getModel() {
		return model;
	}

	public void setModel(CarModel model) {
		this.model = model;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public Set<User> getUsers() {
		return user;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(model.getManufacturer().getName());
		builder.append(" ");
		builder.append(model.getName());
		builder.append(" ");
		builder.append(registrationNumber);
		return builder.toString();
	}
}
