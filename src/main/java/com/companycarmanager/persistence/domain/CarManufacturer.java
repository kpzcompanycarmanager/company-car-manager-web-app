package com.companycarmanager.persistence.domain;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class CarManufacturer {
	@Id
	@GeneratedValue
	public int id;

	@NotEmpty
	private String name;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
