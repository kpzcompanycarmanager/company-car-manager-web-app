package com.companycarmanager.persistence.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.repository.cdi.Eager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.companycarmanager.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User implements UserDetails {
	private static final long serialVersionUID = 1L;

	@Id
	private String name;
	@JsonIgnore
	@Column(nullable = false)
	private String password;
	@Column(nullable = false)
	private String email;
	@Column(nullable = false)
	private boolean enabled;

	@JsonIgnore
	@ManyToOne
	private Car currentCar;

	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<UserRole> roles = new HashSet<UserRole>();

	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Car> availableCars = new HashSet<Car>();

	public User() {
	}

	public User(String name, String password, String email, boolean enabled) {
		this.name = name;
		this.password = password;
		this.enabled = enabled;
		this.email = email;
	}

	public void setFromDto(UserDto userDto) {
		email = userDto.getEmail();
		password = userDto.getPassword();
		enabled = userDto.isEnabled();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean hasAuthority(String authorityName) {
		for (UserRole role : roles) {
			if (role.getName().equals(authorityName)) {
				return true;
			}
		}
		return false;
	}
	@JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (UserRole role : roles) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		return authorities;
	}

	public String getUsername() {
		return name;
	}

	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Car> getAvailableCars() {
		return availableCars;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public Car getCurrentCar() {
		return currentCar;
	}
	
	public void setCurrentCar(Car car) {
		currentCar = car;
	}
}
