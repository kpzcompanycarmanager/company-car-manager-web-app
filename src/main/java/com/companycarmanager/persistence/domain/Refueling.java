package com.companycarmanager.persistence.domain;

import javax.persistence.*;

@Entity
public class Refueling {
	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne
	private Position position;

	@Column(nullable = false)
	float amount;
	@Column(nullable = false)
	float costPerLitre;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getCostPerLitre() {
		return costPerLitre;
	}

	public void setCostPerLitre(float costPerLitre) {
		this.costPerLitre = costPerLitre;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public String amountToString() {
		StringBuilder builder = new StringBuilder();

		builder.append(amount);
		// TODO: Currency handling
		// builder.append(currency)

		return builder.toString();
	}

	public String costPerLitreToString() {
		StringBuilder builder = new StringBuilder();

		builder.append(costPerLitre);
		// TODO: Volume units handling
		// builder.append(units)

		return builder.toString();
	}
}
