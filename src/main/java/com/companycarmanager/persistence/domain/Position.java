package com.companycarmanager.persistence.domain;

import java.util.Date;

import javax.persistence.*;

@Entity
public class Position {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(nullable = false)
	private double longitude;
	@Column(nullable = false)
	private double latitude;

	@ManyToOne
	private User user;
	@ManyToOne
	private Car car;

	@Column(nullable = false)
	private Date date = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(Math.abs(longitude));
		builder.append(longitude > 0 ? "E " : "W ");
		
		builder.append(Math.abs(latitude));
		builder.append(latitude > 0 ? "N" : "S");
		
		return builder.toString();
	}
}
