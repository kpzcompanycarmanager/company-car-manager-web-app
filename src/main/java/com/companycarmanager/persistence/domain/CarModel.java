package com.companycarmanager.persistence.domain;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class CarModel {
	@Id
	@GeneratedValue
	public int id;

	@NotEmpty
	private String name;

	@ManyToOne
	public CarManufacturer manufacturer;

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CarManufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(CarManufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}
}
