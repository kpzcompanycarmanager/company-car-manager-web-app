package com.companycarmanager.persistence.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class UserRole {
	@Id
	@GeneratedValue
	private Integer id;

	@NotNull
	private String name;

	@ManyToMany(mappedBy = "roles")
	private Set<User> users = new HashSet<User>(0);

	public String getName() {
		return name;
	}
}
