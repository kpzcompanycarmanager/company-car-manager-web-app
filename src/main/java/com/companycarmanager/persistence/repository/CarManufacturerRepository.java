package com.companycarmanager.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.companycarmanager.persistence.domain.CarManufacturer;

public interface CarManufacturerRepository extends
		JpaRepository<CarManufacturer, Integer> {

	CarManufacturer findCarManufacturerByName(String name);

}
