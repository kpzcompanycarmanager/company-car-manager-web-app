package com.companycarmanager.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.companycarmanager.persistence.domain.CarModel;

public interface CarModelRepository extends JpaRepository<CarModel, Integer> {
	CarModel findCarModelByName(String name);
}
