package com.companycarmanager.persistence.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.Position;
import com.companycarmanager.persistence.domain.User;

public interface PositionRepository extends JpaRepository<Position, Integer> {
	
	@Query("SELECT p FROM Position p WHERE p.car IN :cars ORDER BY p.date DESC")
	Set<Position> findCarPositions(@Param("cars") Set<Car> cars);
	
	List<Position> findPositionByCarOrderByDateDesc(Car car);

	List<Position> findPositionByUserOrderByDateDesc(User user);
}
