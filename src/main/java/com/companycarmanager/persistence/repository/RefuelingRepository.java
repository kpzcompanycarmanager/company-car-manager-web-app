package com.companycarmanager.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.Refueling;
import com.companycarmanager.persistence.domain.User;

public interface RefuelingRepository extends JpaRepository<Refueling, Integer> {

	List<Refueling> findRefuelingByPositionCarOrderByPositionDateDesc(Car car);

	List<Refueling> findRefuelingByPositionUserOrderByPositionDateDesc(User user);

	//List<Refueling> findRefuelingByUser(User user);
}