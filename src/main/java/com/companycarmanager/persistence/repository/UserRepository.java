package com.companycarmanager.persistence.repository;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.User;

public interface UserRepository extends JpaRepository<User, String> {
	@ManyToMany(mappedBy = "availableCars", cascade = CascadeType.PERSIST)
	@Query("SELECT u FROM User u WHERE u.name NOT IN "
			+ "(SELECT u.name FROM u.availableCars ac WHERE ac.id = :carId) GROUP BY u.name")
	public List<User> findUsersWithoutCarAssigned(@Param("carId") Integer carId);

	@Query("SELECT c FROM Car c WHERE c.id IN (SELECT ac.id FROM User u LEFT JOIN u.availableCars ac WHERE u.name = :userName)")
	public List<Car> findAssignedCars(@Param("userName") String userName);

	public User findUserByEmail(String email);
	
	public User findUserByCurrentCar(Car currentCar);
}