package com.companycarmanager.persistence.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.companycarmanager.persistence.domain.Car;

public interface CarRepository extends JpaRepository<Car, Integer> {

	@Query("SELECT c FROM Car c WHERE c.id NOT IN (SELECT u.currentCar.id FROM User u WHERE u.currentCar != null)")
	Set<Car> findCarsWithoutCurrentUser();
	
	@Query("SELECT c FROM Car c WHERE c.id NOT IN (SELECT u.currentCar.id FROM User u WHERE u.currentCar != null) AND c.id IN (SELECT c FROM Car c WHERE c.id IN (SELECT ac.id FROM User u LEFT JOIN u.availableCars ac WHERE u.name = :userName)) AND c.available = true")
	Set<Car> findCarsWithoutCurrentUserAvailableToUser(@Param("userName")String userName);
	
	Car findCarByRegistrationNumber(String registrationNumber);

	Set<Car> findCarByAvailableTrue();
}