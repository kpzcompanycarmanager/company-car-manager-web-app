package com.companycarmanager.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.companycarmanager.persistence.domain.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {
	UserRole findUserRoleByName(String name);
}