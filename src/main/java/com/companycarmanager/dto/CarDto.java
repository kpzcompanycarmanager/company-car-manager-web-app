package com.companycarmanager.dto;


import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.companycarmanager.persistence.domain.Car;

public class CarDto {
	@NotEmpty
	private String registrationNumber;
	
	
	private Integer id;
	
	@NotNull
	private Integer modelId;
	
	private boolean available = true;

	public CarDto() { }
	
	public CarDto(Car car) {
		fillFromCar(car);
	}

	public void fillFromCar(Car car) {
		this.id = car.getId();
		this.modelId = car.getModel().getId();
		this.registrationNumber = new String(car.getRegistrationNumber());
		this.available = car.isAvailable();
	}
	
	public Integer getModelId() {
		return modelId;
	}
	
	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}
	
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	
	public boolean getAvailable() {
		return available;
	}
	
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}	
}
