package com.companycarmanager.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;



public class RefuelingDto {
	@NotNull
	@Min(value = 0)
	private Integer positionId;
	@Min(value = 0)
	float amount;
	@Min(value = 0)
	float costPerLitre;

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getCostPerLitre() {
		return costPerLitre;
	}

	public void setCostPerLitre(float costPerLitre) {
		this.costPerLitre = costPerLitre;
	}

	public Integer getPositionId() {
		return positionId;
	}

	public void setPositionId(Integer positionId) {
		this.positionId = positionId;
	}
	
}

