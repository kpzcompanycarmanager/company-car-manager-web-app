package com.companycarmanager.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

public class PositionDto {
	@NotNull
	@DecimalMax(value= "180.0")
	@DecimalMin(value= "-180.0")
	private BigDecimal longitude;
	
	@DecimalMax(value= "90.0")
	@DecimalMin(value= "-90.0")
	private BigDecimal latitude;

	@NotNull
	private String userName;
	@NotNull
	private Integer carId;

	private Date date = new Date();

	public double getLongitude() {
		return longitude.doubleValue();
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude.doubleValue();
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public Integer getCarId() {
		return carId;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}

