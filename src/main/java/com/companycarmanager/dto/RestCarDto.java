package com.companycarmanager.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.companycarmanager.persistence.domain.Car;
import com.companycarmanager.persistence.domain.CarModel;

public class RestCarDto {
	@NotEmpty
	private String registrationNumber;

	private Integer id;

	@NotNull
	private CarModel model;

	public RestCarDto() {
	}

	public RestCarDto(Car car) {
		fillFromCar(car);
	}

	public void fillFromCar(Car car) {
		this.id = car.getId();
		this.model = car.getModel();
		this.registrationNumber = new String(car.getRegistrationNumber());
	}

	public CarModel getModel() {
		return model;
	}

	public void setModel(CarModel model) {
		this.model = model;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
