package com.companycarmanager.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.companycarmanager.persistence.domain.User;
import com.companycarmanager.validators.PasswordMatches;
import com.companycarmanager.validators.ValidEmail;

//@PasswordMatches
public class UserDto {
	@NotNull
	@NotEmpty
	private String userName;

	@NotNull
	@NotEmpty
	private String password;
	private String matchingPassword;

	@NotNull
	@NotEmpty
	private String email;

	@NotNull
	private boolean enabled = true;

	private Integer currentCarId = null;

	public UserDto() {
	}

	public void fillFromUser(User user) {
		this.userName = new String(user.getName());
		this.email = new String(user.getEmail());
		this.enabled = user.isEnabled();
		this.currentCarId = user.getCurrentCar() != null ? user.getCurrentCar().getId() : null;
	}

	public UserDto(User user) {
		fillFromUser(user);
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Integer getCurrentCarId() {
		return currentCarId;
	}
	
	public void setCurrentCarId(Integer carId) {
		currentCarId = carId;
		
	}
}