package com.companycarmanager.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class CarModelDto {
	@NotEmpty
	private String name;

	public int manufacturerId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(int manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
}
